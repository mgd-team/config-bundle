<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 11.09.17
 * Time: 6:09 PM.
 */

namespace MGD\ConfigBundle\Factory;

use MGD\ConfigBundle\Admin\Configurator\ValueAdminConfiguratorInterface;
use MGD\ConfigBundle\Model\BaseConfig;

class ValueConfiguratorFactory
{
    /**
     * @var ValueAdminConfiguratorInterface[]
     */
    protected $configurators;

    /**
     * @param ValueAdminConfiguratorInterface $configurator
     */
    public function addConfigurator(ValueAdminConfiguratorInterface $configurator): void
    {
        $this->configurators[] = $configurator;
    }

    /**
     * @param BaseConfig $config
     *
     * @throws \RuntimeException
     *
     * @return ValueAdminConfiguratorInterface
     */
    public function create(BaseConfig $config)
    {
        foreach ($this->configurators as $configurator) {
            $target = $configurator->getTarget();
            if ($config instanceof $target) {
                return clone $configurator;
            }
        }

        throw new \RuntimeException('Cannot find configurator for '.get_class($config));
    }
}
