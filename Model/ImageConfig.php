<?php

//set your namespace
namespace MGD\ConfigBundle\Model;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table("config_image")
 */
class ImageConfig extends \MGD\ConfigBundle\Model\BaseImageConfig
{
}
