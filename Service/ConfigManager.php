<?php

namespace MGD\ConfigBundle\Service;

use MGD\ConfigBundle\Model\BaseConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use MGD\ConfigBundle\Model\ConfigInterface;

class ConfigManager
{
    /**
     * @var ConfigInterface[]
     */
    private $configs;
    /**
     * @var PropertyAccessor
     */
    private $propertyAccessor;
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Use local cache to prevent multiple database queries.
     * Instead of load each config value separately the manager preloads
     * all configs to local array. It may be not acceptable for daemon process.
     *
     * @var bool
     */
    private $useCache = true;

    /**
     * ConfigManager constructor.
     *
     * @param PropertyAccessor   $propertyAccessor
     * @param ContainerInterface $container
     */
    public function __construct(PropertyAccessor $propertyAccessor, ContainerInterface $container)
    {
        $this->propertyAccessor = $propertyAccessor;
        $this->container = $container;
    }

    /**
     * @return array|ConfigInterface[]
     */
    private function getConfigs()
    {
        if (null === $this->configs || !$this->useCache) {
            $this->configs = $this
                ->container
                ->get('doctrine.orm.entity_manager')
                ->getRepository(BaseConfig::class)
                ->findAll();
        }

        return $this->configs;
    }

    /**
     * @param string $attribute
     * @param $value
     *
     * @return ConfigInterface|null
     */
    private function get(string $attribute, $value): ?ConfigInterface
    {
        $configs = $this->getConfigs();
        if ($configs) {
            foreach ($configs as $config) {
                if ($this->propertyAccessor->isReadable($config, $attribute)
                && $this->propertyAccessor->getValue($config, $attribute) == $value) {
                    return $config;
                }
            }
        }

        return null;
    }

    public function disableCache()
    {
        $this->useCache = false;
    }

    public function enableCache()
    {
        $this->useCache = true;
    }

    /**
     * @param string $code
     *
     * @return ConfigInterface|null
     */
    public function getByCode(string $code): ?ConfigInterface
    {
        return $this->get('code', $code);
    }

    /**
     * @param array $config - array with fields: ['class' => '', 'code' => '', 'value' => '', 'name' => '']
     *
     * @return ?ConfigInterface
     */
    public function add(array $config): ?ConfigInterface
    {
        $manager = $this->container->get('doctrine.orm.entity_manager');

        $configObject = new $config['class']();

        if ($configObject instanceof BaseConfig) {
            $configObject->setName($config['name']);
            $configObject->setValue($config['value']);
            $configObject->setCode($config['code']);

            $manager->persist($configObject);

            $this->configs[] = $configObject;

            return $configObject;
        }

        return null;
    }
}
