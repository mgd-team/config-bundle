<?php

namespace MGD\ConfigBundle\Admin\Configurator;

use MGD\ConfigBundle\Model\IntegerConfig;
use MGD\ConfigBundle\Model\StringConfig;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class IntegerConfigurator implements ValueAdminConfiguratorInterface
{
    public function configureFormFields(FormMapper $formMapper, AbstractAdmin $context)
    {
        $formMapper
            ->add('value', IntegerType::class);
    }

    public static function getTarget(): string
    {
        return IntegerConfig::class;
    }

    public static function getTemplate(): string
    {
        return 'MGDConfigBundle::default_value_field.html.twig';
    }
}
