<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 31.03.18
 * Time: 6:05
 */

namespace MGD\ConfigBundle\Admin\Configurator;

use MGD\ConfigBundle\Model\BooleanConfig;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\Form\Type\BooleanType;

class BooleanConfigurator implements ValueAdminConfiguratorInterface
{
    public function configureFormFields(FormMapper $formMapper, AbstractAdmin $context)
    {
        $formMapper
            ->add('value', BooleanType::class, [
                'transform' => true,
            ]);
    }

    public static function getTarget(): string
    {
        return BooleanConfig::class;
    }

    public static function getTemplate(): string
    {
        return '@SonataAdminBundle/CRUD/list_boolean.html.twig';
    }
}
