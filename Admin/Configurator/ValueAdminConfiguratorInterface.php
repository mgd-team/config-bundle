<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 11.09.17
 * Time: 6:02
 */

namespace MGD\ConfigBundle\Admin\Configurator;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;

interface ValueAdminConfiguratorInterface
{
    public function configureFormFields(FormMapper $formMapper, AbstractAdmin $context);

    public static function getTarget(): string;

    public static function getTemplate(): string;
}
