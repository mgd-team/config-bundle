<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 11.09.17
 * Time: 6:05
 */

namespace MGD\ConfigBundle\Admin\Configurator;

use MGD\ConfigBundle\Model\DateTimeConfig;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;

class DateTimeConfigurator implements ValueAdminConfiguratorInterface
{
    public function configureFormFields(FormMapper $formMapper, AbstractAdmin $context)
    {
        $formMapper
            ->add('value', DateTimePickerType::class, array(
                'format' => 'dd-MM-Y H:m'
            ));
    }

    public static function getTarget(): string
    {
        return DateTimeConfig::class;
    }

    public static function getTemplate(): string
    {
        return '@MGDConfigBundle/datetime_value_field.html.twig';
    }
}
