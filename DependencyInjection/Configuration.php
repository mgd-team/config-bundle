<?php

namespace MGD\ConfigBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('mgd_config');
        $rootNode = $treeBuilder->getRootNode('mgd_config');

        $rootNode
            ->children()
                ->scalarNode('admin_class')->defaultValue('MGD\ConfigBundle\Admin\ConfigAdmin')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
